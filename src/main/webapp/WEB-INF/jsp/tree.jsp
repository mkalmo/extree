<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="styles.jsp"%>
<body>
<%@ include file="menu.jsp"%>
<div id="tree">
<c:forEach var="each" items="${unitRows}">
  <my:repeat what="&nbsp;" times="${each.level * 3}" /><c:out
    value="${each.unit.code}"></c:out><br></c:forEach>
</div>
</body>
</html>