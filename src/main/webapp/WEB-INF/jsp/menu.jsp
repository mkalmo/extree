<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<ul id="menu">
    <li><a href="<c:url value='/' />"><spring:message code="menu.tree" /></a></li>
    <li><a href="<c:url value='/addForm' />"><spring:message code="menu.addForm" /></a></li>
    <li><a href="?lang=en"><spring:message code="menu.lang.en" /></a></li>
    <li><a href="?lang=et"><spring:message code="menu.lang.et" /></a></li>
</ul>
<br /><br /><br />
