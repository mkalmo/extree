<%@ tag language="java" trimDirectiveWhitespaces="true"
        pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ attribute name="what" %>
<%@ attribute name="times" %>
<c:forEach begin="1" end="${times}">${what}</c:forEach>