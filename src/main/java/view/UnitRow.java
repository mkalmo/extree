package view;

import model.Unit;

public class UnitRow {

    private int level;

    private Unit unit;

    public UnitRow(Unit unit, int level) {
        this.unit = unit;
        this.level = level;
    }

    @Override
    public String toString() {
        return "UnitRow [level=" + level + ", unit=" + unit + "]";
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

}
