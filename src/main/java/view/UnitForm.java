package view;

import javax.validation.Valid;

import model.Unit;

public class UnitForm {

    @Valid
    private Unit unit;

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

}
