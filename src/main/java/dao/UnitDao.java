package dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.Unit;

import org.springframework.stereotype.Repository;

import view.UnitRow;

@Repository
public class UnitDao {

    private Long c = 1L;

    public List<Unit> getRootUnits() {

        Unit r1 = new Unit(c++, "1",
                        new Unit(c++, "1-1",
                                new Unit(c++, "1-1-1"),
                                new Unit(c++, "1-1-2")),
                        new Unit(c++, "1-2"));

        Unit r2 = new Unit(c++, "2");

        return Arrays.asList(r1, r2);
    }

    public List<UnitRow> getUnitRows() {
        List<UnitRow> list = new ArrayList<UnitRow>();

        collectAllNodesInto(list, getRootUnits(), 0);

        return list;
    }

    private void collectAllNodesInto(List<UnitRow> list,
            List<Unit> rootUnits, int level) {

        for (Unit unit : rootUnits) {
            list.add(new UnitRow(unit, level));
            collectAllNodesInto(list, unit.getSubordinates(), level + 1);
        }
    }



}
