package controller;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import view.UnitForm;
import dao.UnitDao;

@Controller
public class UnitController {

    @Resource
    private UnitDao unitDao;

    @RequestMapping("/")
    public String tree(ModelMap model) {

        model.addAttribute("unitRows", unitDao.getUnitRows());

        return "tree";
    }

    @RequestMapping("/addForm")
    public String showForm(@ModelAttribute UnitForm form) {
        return "form";
    }

    @RequestMapping(value="/save", method = RequestMethod.POST)
    public String saveForm(ModelMap model,
            @ModelAttribute @Valid UnitForm form, BindingResult result) {

        return "form";
    }

    @RequestMapping(value="/save")
    public String saveForm(UnitForm form) {
        return "redirect:/addForm";
    }

}