package model;

import java.util.*;

import javax.validation.constraints.Pattern;

public class Unit {

    private Long id;

    @Pattern(regexp = "[0-9]+")
    private String code;

    private Unit superUnit;

    private List<Unit> subordinates = new ArrayList<Unit>();

    public Unit() {}

    public Unit(Long id, String code, Unit ... subordinates) {
        this.id = id;
        this.code = code;
        for (Unit unit : subordinates) {
            this.subordinates.add(unit);
            unit.superUnit = this;
        }
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    @Override
    public String toString() {
        return "Unit [code=" + code + "]";
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public List<Unit> getSubordinates() {
        return subordinates;
    }
    public void setSubordinates(List<Unit> subordinates) {
        this.subordinates = subordinates;
    }
    public Unit getSuperUnit() {
        return superUnit;
    }
    public void setSuperUnit(Unit superUnit) {
        this.superUnit = superUnit;
    }
}
